package ru.vmaksimenkov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.api.IRepository;
import ru.vmaksimenkov.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    boolean existsByEmail(@Nullable String email);

    boolean existsById(@NotNull String id);

    boolean existsByLogin(@Nullable String login);

    @Nullable
    User findByLogin(@NotNull String login);

    void removeByLogin(@NotNull String login);

    void setPasswordById(@NotNull String id, @NotNull String password);

}
