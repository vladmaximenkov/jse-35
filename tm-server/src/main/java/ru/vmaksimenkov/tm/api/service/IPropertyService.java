package ru.vmaksimenkov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.vmaksimenkov.tm.api.other.ISaltSettings;

public interface IPropertyService extends ISaltSettings {

    @NotNull String getApplicationVersion();

    int getBackupInterval();

    int getScannerInterval();

    @NotNull String getServerHost();

    int getServerPort();

    int getSessionCycle();

    @NotNull String getSessionSalt();

}
