package ru.vmaksimenkov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.api.repository.IUserRepository;
import ru.vmaksimenkov.tm.model.User;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public boolean existsByEmail(@Nullable final String email) {
        if (email == null) return false;
        return list.stream()
                .anyMatch(e -> email.equals(e.getEmail()));
    }

    @Override
    public boolean existsById(@NotNull String id) {
        return list.stream()
                .anyMatch(e -> id.equals(e.getId()));
    }

    @Override
    public boolean existsByLogin(@Nullable final String login) {
        if (login == null) return false;
        return list.stream()
                .anyMatch(e -> login.equals(e.getLogin()));
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        return list.stream()
                .filter(e -> login.equals(e.getLogin()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public void removeByLogin(@NotNull final String login) {
        remove(list.stream()
                .filter(e -> login.equals(e.getLogin()))
                .findFirst()
                .orElse(null)
        );
    }

    @Override
    public void setPasswordById(@NotNull final String id, @NotNull final String passwordHash) {
        @Nullable final User user = findById(id);
        if (user == null) return;
        user.setPasswordHash(passwordHash);
    }

}
