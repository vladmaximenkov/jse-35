package ru.vmaksimenkov.tm.bootstrap;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.vmaksimenkov.tm.api.repository.ICommandRepository;
import ru.vmaksimenkov.tm.api.service.EndpointLocator;
import ru.vmaksimenkov.tm.api.service.ICommandService;
import ru.vmaksimenkov.tm.api.service.ILoggerService;
import ru.vmaksimenkov.tm.api.service.IPropertyService;
import ru.vmaksimenkov.tm.command.AbstractCommand;
import ru.vmaksimenkov.tm.component.FileScanner;
import ru.vmaksimenkov.tm.endpoint.*;
import ru.vmaksimenkov.tm.exception.system.UnknownCommandException;
import ru.vmaksimenkov.tm.repository.CommandRepository;
import ru.vmaksimenkov.tm.service.CommandService;
import ru.vmaksimenkov.tm.service.LoggerService;
import ru.vmaksimenkov.tm.service.PropertyService;
import ru.vmaksimenkov.tm.util.SystemUtil;
import ru.vmaksimenkov.tm.util.TerminalUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

import static ru.vmaksimenkov.tm.util.ValidationUtil.isEmpty;

@Getter
public final class Bootstrap implements EndpointLocator {

    @NotNull
    private final static String PID_FILENAME = "task-manager.pid";
    @NotNull
    private final AdminEndpointService adminEndpointService = new AdminEndpointService();
    @NotNull
    private final AdminEndpoint adminEndpoint = adminEndpointService.getAdminEndpointPort();
    @NotNull
    private final AdminUserEndpointService adminUserEndpointService = new AdminUserEndpointService();
    @NotNull
    private final AdminUserEndpoint adminUserEndpoint = adminUserEndpointService.getAdminUserEndpointPort();
    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);
    @NotNull
    private final ILoggerService loggerService = new LoggerService();
    @NotNull
    private final ProjectEndpointService projectEndpointService = new ProjectEndpointService();
    @NotNull
    private final ProjectEndpoint projectEndpoint = projectEndpointService.getProjectEndpointPort();
    @NotNull
    private final IPropertyService propertyService = new PropertyService();
    @NotNull
    private final FileScanner fileScanner = new FileScanner(this, propertyService);
    @NotNull
    private final SessionEndpointService sessionEndpointService = new SessionEndpointService();
    @NotNull
    private final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();
    @NotNull
    private final TaskEndpointService taskEndpointService = new TaskEndpointService();
    @NotNull
    private final TaskEndpoint taskEndpoint = taskEndpointService.getTaskEndpointPort();
    @NotNull
    private final UserEndpointService userEndpointService = new UserEndpointService();
    @NotNull
    private final UserEndpoint userEndpoint = userEndpointService.getUserEndpointPort();
    @Nullable
    private Session session = null;

    private void init() {
        initCommands();
        initPID();
        fileScanner.init();
    }

    @SneakyThrows
    private void initCommands() {
        @NotNull final Reflections reflections = new Reflections("ru.vmaksimenkov.tm.command");
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) {
            final boolean isAbstract = Modifier.isAbstract(clazz.getModifiers());
            if (isAbstract) continue;
            registry(clazz.newInstance());
        }
    }

    @SneakyThrows
    public void initPID() {
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(PID_FILENAME), pid.getBytes());
        @NotNull final File file = new File(PID_FILENAME);
        file.deleteOnExit();
    }

    public void parseArg(@Nullable final String arg) {
        if (isEmpty(arg)) return;
        @Nullable final AbstractCommand command = commandService.getCommandByArg(arg);
        if (command == null) return;
        command.execute();
    }

    public boolean parseArgs(@Nullable final String[] args) {
        if (args == null || args.length < 1) return false;
        @Nullable final String arg = args[0];
        parseArg(arg);
        return true;
    }

    public void parseCommand(@Nullable final String cmd) {
        if (isEmpty(cmd)) return;
        @Nullable final AbstractCommand command = commandService.getCommandByName(cmd);
        if (command == null) throw new UnknownCommandException(cmd);
        command.execute();
    }

    private void registry(@Nullable final AbstractCommand command) {
        if (command == null) return;
        command.setEndpointLocator(this);
        commandService.add(command);
    }

    public void run(@Nullable final String... args) {
        loggerService.debug("Debug mode");
        loggerService.info("** WELCOME TO TASK MANAGER **");
        if (parseArgs(args)) System.exit(0);
        init();
        //noinspection InfiniteLoopStatement
        while (true) {
            System.out.println("Enter command: ");
            @Nullable final String command = TerminalUtil.nextLine();
            loggerService.command(command);
            try {
                parseCommand(command);
                System.err.println("[OK]");
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    public void setSession(@NotNull final Session session) {
        this.session = session;
    }

}
