package ru.vmaksimenkov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.vmaksimenkov.tm.command.AbstractCommand;

public final class HelpCommand extends AbstractCommand {

    @NotNull
    @Override
    public String commandArg() {
        return "-h";
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Show all commands";
    }

    @NotNull
    @Override
    public String commandName() {
        return "help";
    }

    @Override
    public void execute() {
        //TODO
//        System.out.println("[HELP]");
//        @Nullable final Collection<AbstractCommand> commands = endpointLocator.getCommandService().getCommands();
//        assert commands != null;
//        commands.forEach(e -> System.out.println(e.commandName() + ": " + e.commandDescription()));
    }

}