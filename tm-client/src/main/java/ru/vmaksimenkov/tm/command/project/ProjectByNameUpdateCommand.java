package ru.vmaksimenkov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.exception.empty.EmptyNameException;
import ru.vmaksimenkov.tm.exception.entity.ProjectNotFoundException;
import ru.vmaksimenkov.tm.util.TerminalUtil;

import static ru.vmaksimenkov.tm.util.ValidationUtil.isEmpty;

public final class ProjectByNameUpdateCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Update project by name";
    }

    @NotNull
    @Override
    public String commandName() {
        return "project-update-by-name";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("[ENTER NAME:]");
        @NotNull final String name = TerminalUtil.nextLine();
        if (!endpointLocator.getProjectEndpoint().existsProjectByName(endpointLocator.getSession(), name))
            throw new ProjectNotFoundException();
        System.out.println("ENTER NEW NAME:");
        @NotNull final String nameNew = TerminalUtil.nextLine();
        if (isEmpty(nameNew)) throw new EmptyNameException();
        System.out.println("ENTER DESCRIPTION:");
        endpointLocator.getProjectEndpoint().updateProjectByName(endpointLocator.getSession(), name, nameNew, TerminalUtil.nextLine());
    }

}
