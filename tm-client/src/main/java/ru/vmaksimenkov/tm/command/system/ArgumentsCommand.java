package ru.vmaksimenkov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.vmaksimenkov.tm.command.AbstractCommand;

public final class ArgumentsCommand extends AbstractCommand {

    @NotNull
    @Override
    public String commandArg() {
        return "-a";
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Show all arguments";
    }

    @NotNull
    @Override
    public String commandName() {
        return "arguments";
    }

    @Override
    public void execute() {
        //TODO
//        System.out.println("[ARGUMENTS]");
//        @Nullable final Collection<AbstractCommand> arguments = endpointLocator.getCommandService().getArguments();
//        assert arguments != null;
//        arguments.forEach(e -> System.out.println(e.commandArg()));
    }

}
