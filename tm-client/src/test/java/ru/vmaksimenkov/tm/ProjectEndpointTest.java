package ru.vmaksimenkov.tm;

import com.sun.xml.ws.fault.ServerSOAPFaultException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.vmaksimenkov.tm.bootstrap.Bootstrap;
import ru.vmaksimenkov.tm.endpoint.Project;
import ru.vmaksimenkov.tm.endpoint.Session;
import ru.vmaksimenkov.tm.marker.SoapCategory;

public class ProjectEndpointTest {

    @NotNull
    private static final Bootstrap BOOTSTRAP = new Bootstrap();

    @Nullable
    private static Session SESSION = BOOTSTRAP.getSessionEndpoint().openSession("test", "test");

    @AfterClass
    public static void after() {
        BOOTSTRAP.getProjectEndpoint().clearProject(SESSION);
        BOOTSTRAP.getSessionEndpoint().closeSession(SESSION);
    }

    @Test(expected = ServerSOAPFaultException.class)
    @Category(SoapCategory.class)
    public void clearProjectsWithInvalidSession() {
        @NotNull final Session emptySession = new Session();
        BOOTSTRAP.getProjectEndpoint().clearProject(emptySession);
    }

    @Test
    @Category(SoapCategory.class)
    public void createTest() {
        @NotNull final Project project = BOOTSTRAP.getProjectEndpoint().createProject(SESSION, "DEMO", "DEMO DESCRIPTION");
        Assert.assertNotNull(project);
        Assert.assertEquals("DEMO", project.getName());
    }

}
