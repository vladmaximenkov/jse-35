package ru.vmaksimenkov.tm;

import com.sun.xml.ws.fault.ServerSOAPFaultException;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.vmaksimenkov.tm.endpoint.Session;
import ru.vmaksimenkov.tm.endpoint.SessionEndpoint;
import ru.vmaksimenkov.tm.endpoint.SessionEndpointService;
import ru.vmaksimenkov.tm.endpoint.User;
import ru.vmaksimenkov.tm.marker.SoapCategory;

public class SessionEndpointTest {

    @NotNull
    private final SessionEndpointService sessionEndpointService = new SessionEndpointService();

    @NotNull
    private final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

    @Test(expected = ServerSOAPFaultException.class)
    @Category(SoapCategory.class)
    public void testCloseSession() {
        @NotNull final Session session = sessionEndpoint.openSession("test", "test");
        Assert.assertNotNull(session);
        @NotNull final User user = sessionEndpoint.getUser(session);
        Assert.assertNotNull(user);
        Assert.assertEquals("test", user.getLogin());
        sessionEndpoint.closeSession(session);
        Assert.assertNull(sessionEndpoint.getUser(session));
    }

    @Test
    @Category(SoapCategory.class)
    public void testIncorrect() {
        @NotNull final Session session = sessionEndpoint.openSession("qweqwe", "123123");
        Assert.assertNull(session);
    }

    @Test
    @Category(SoapCategory.class)
    public void testOpenSession() {
        @NotNull final Session session = sessionEndpoint.openSession("test", "test");
        Assert.assertNotNull(session);
        @NotNull final User user = sessionEndpoint.getUser(session);
        Assert.assertNotNull(user);
        Assert.assertEquals("test", user.getLogin());
    }

}
